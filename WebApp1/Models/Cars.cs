﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApp1.Models
{
    public class Cars
    {
        public int id { get; set; }
        public string type { get; set; }        //Тип – грузовой , легковой
        public string model { get; set; }       //Марка – камаз, DAF, MAN , ….
        public int power { get; set; }          //Мощность двигателя - ххх л.с.
        public DateTime dateprod { get; set; }  //Год выпуска – хххх
        public string vin { get; set; }         //VIN код – хххххххххх
        public DateTime startuse { get; set; }  //Дата начала эксплуатации
        public DateTime enduse { get; set; }    //Дата окончания эксплуатации


    }
}
