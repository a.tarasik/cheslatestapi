﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using WebApp1.Models;

namespace WebApp1.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class CarsController : ControllerBase
    {
        private readonly IConfiguration _configuration;
        public CarsController(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        [HttpGet]
        #region get all cars      
        //GET  http://localhost:62841/Cars
        public JsonResult GetAllCars()
        {
            string query = @"
                select id as ""id"",
                        type as ""type"",
                        model as ""model"",
                        power as ""power"",
                        to_char(dateprod, 'YYYY-MM-DD') as ""dateprod"",
                        vin as ""vin"",
                        to_char(startuse, 'YYYY-MM-DD') as ""startuse"",
                        to_char(enduse, 'YYYY-MM-DD') as ""enduse""
                from public.""Cars""
                order by id asc
            ";

            DataTable table = new DataTable();
            string sqlDataSource = _configuration.GetConnectionString("CarsAppCon");
            NpgsqlDataReader myReader;
            using (NpgsqlConnection myCon = new NpgsqlConnection(sqlDataSource))
            {
                myCon.Open();
                using (NpgsqlCommand myCommand = new NpgsqlCommand(query, myCon))
                {
                    myReader = myCommand.ExecuteReader();
                    table.Load(myReader);

                    myReader.Close();
                    myCon.Close();
                }
            }
            return new JsonResult(table);
        }
        #endregion

        [HttpPut]
        #region update Car information
        //PUT http://localhost:62841/Cars
        public JsonResult Put(Cars car)
        {
            string query = @"
                update public.""Cars""
                set type = @type, 
                model = @model, 
                power = @power, 
                dateprod = @dateprod, 
                vin = @vin, 
                startuse = @startuse, 
                enduse = @enduse
                where id = @id and (@enduse::date > @startuse::date or @enduse::date = '0001-01-01' or @enduse::date = null) and @startuse::date > @dateprod::date
            ";

            DataTable table = new DataTable();
            string sqlDataSource = _configuration.GetConnectionString("CarsAppCon");
            NpgsqlDataReader myReader;
            using (NpgsqlConnection myCon = new NpgsqlConnection(sqlDataSource))
            {
                myCon.Open();
                using (NpgsqlCommand myCommand = new NpgsqlCommand(query, myCon))
                {
                    myCommand.Parameters.AddWithValue("@id", car.id);
                    myCommand.Parameters.AddWithValue("@type", car.type);
                    myCommand.Parameters.AddWithValue("@model", car.model);
                    myCommand.Parameters.AddWithValue("@power", car.power);
                    myCommand.Parameters.AddWithValue("@dateprod", Convert.ToDateTime(car.dateprod));
                    myCommand.Parameters.AddWithValue("@vin", car.vin);
                    myCommand.Parameters.AddWithValue("@startuse", Convert.ToDateTime(car.startuse));
                    myCommand.Parameters.AddWithValue("@enduse", Convert.ToDateTime(car.enduse));
                    myReader = myCommand.ExecuteReader();
                    table.Load(myReader);

                    myReader.Close();
                    myCon.Close();
                }
            }
            return new JsonResult("Updated successfully!");
        }
        #endregion

        [HttpDelete("{id}")]
        #region delete selected Car
        //DELETE http://localhost:62841/Cars/2
        public JsonResult Delete(int id)
        {
            string query = @"
                delete from public.""Cars""
                where id = @id
            ";

            DataTable table = new DataTable();
            string sqlDataSource = _configuration.GetConnectionString("CarsAppCon");
            NpgsqlDataReader myReader;
            using (NpgsqlConnection myCon = new NpgsqlConnection(sqlDataSource))
            {
                myCon.Open();
                using (NpgsqlCommand myCommand = new NpgsqlCommand(query, myCon))
                {
                    myCommand.Parameters.AddWithValue("@id", id);
                    myReader = myCommand.ExecuteReader();
                    table.Load(myReader);

                    myReader.Close();
                    myCon.Close();
                }
            }
            return new JsonResult("Deleted successfully!");
        }
        #endregion

        [HttpPost]
        #region add new car to table Cars
        //POST http://localhost:62841/Cars
        public JsonResult Post(Cars car)
        {
            string query = @"
                insert into public.""Cars""(type,model,power,dateprod,vin,startuse,enduse)
                values (@type,@model,@power,@dateprod,@vin,@startuse,@enduse)
            ";

            DataTable table = new DataTable();
            string sqlDataSource = _configuration.GetConnectionString("CarsAppCon");
            NpgsqlDataReader myReader;
            using (NpgsqlConnection myCon = new NpgsqlConnection(sqlDataSource))
            {
                myCon.Open();
                using (NpgsqlCommand myCommand = new NpgsqlCommand(query, myCon))
                {
                    myCommand.Parameters.AddWithValue("@type", car.type);
                    myCommand.Parameters.AddWithValue("@model", car.model);
                    myCommand.Parameters.AddWithValue("@power", car.power);
                    myCommand.Parameters.AddWithValue("@dateprod", Convert.ToDateTime(car.dateprod));
                    myCommand.Parameters.AddWithValue("@vin", car.vin);
                    myCommand.Parameters.AddWithValue("@startuse", Convert.ToDateTime(car.startuse));
                    myCommand.Parameters.AddWithValue("@enduse", Convert.ToDateTime(car.enduse));
                    myReader = myCommand.ExecuteReader();
                    table.Load(myReader);

                    myReader.Close();
                    myCon.Close();
                }
            }
            return new JsonResult("Added successfully");
        }
        #endregion

        [HttpGet]
        [Route("GetDecommissionCars")]
        #region GetDecommissionCars
        //GET http://localhost:62841/Cars/GetDecommissionCars
        public JsonResult GetDecommissionCars()
        {
            string query = @"
                select id as ""id"",
                        type as ""type"",
                        model as ""model"",
                        power as ""power"",
                        to_char(dateprod, 'YYYY-MM-DD') as ""dateprod"",
                        vin as ""vin"",
                        to_char(startuse, 'YYYY-MM-DD') as ""startuse"",
                        to_char(enduse, 'YYYY-MM-DD') as ""enduse""
                from public.""Cars""
                where enduse is not null AND enduse::date != '0001-01-01'
                order by id asc
            ";

            DataTable table = new DataTable();
            string sqlDataSource = _configuration.GetConnectionString("CarsAppCon");
            NpgsqlDataReader myReader;
            using (NpgsqlConnection myCon = new NpgsqlConnection(sqlDataSource))
            {
                myCon.Open();
                using (NpgsqlCommand myCommand = new NpgsqlCommand(query, myCon))
                {
                    myReader = myCommand.ExecuteReader();
                    table.Load(myReader);

                    myReader.Close();
                    myCon.Close();
                }
            }
            return new JsonResult(table);
        }
        #endregion

        [HttpGet]
        [Route("GetWorkingCars")]
        #region GetWorkingCars
        //GET http://localhost:62841/Cars/GetWorkingCars
        public JsonResult GetWorkingCars()
        {
            string query = @"
                select id as ""id"",
                        type as ""type"",
                        model as ""model"",
                        power as ""power"",
                        to_char(dateprod, 'YYYY-MM-DD') as ""dateprod"",
                        vin as ""vin"",
                        to_char(startuse, 'YYYY-MM-DD') as ""startuse"",
                        to_char(enduse, 'YYYY-MM-DD') as ""enduse""
                from public.""Cars""
                where enduse::date = '0001-01-01' or enduse is null or enduse::date = null
            ";

            DataTable table = new DataTable();
            string sqlDataSource = _configuration.GetConnectionString("CarsAppCon");
            NpgsqlDataReader myReader;
            using (NpgsqlConnection myCon = new NpgsqlConnection(sqlDataSource))
            {
                myCon.Open();
                using (NpgsqlCommand myCommand = new NpgsqlCommand(query, myCon))
                {
                    myReader = myCommand.ExecuteReader();
                    table.Load(myReader);

                    myReader.Close();
                    myCon.Close();
                }
            }
            return new JsonResult(table);
        }
        #endregion

        [HttpGet]
        [Route("GetCarByVIN/{vin}")]
        #region GetCarByVIN
        //GET http://localhost:62841/Cars/GetCarByVIN/EF2015ASebdE5321584
        public JsonResult GetCarByVIN(string VIN)
        {
            string query = @"
                select id as ""id"",
                        type as ""type"",
                        model as ""model"",
                        power as ""power"",
                        to_char(dateprod, 'YYYY-MM-DD') as ""dateprod"",
                        vin as ""vin"",
                        to_char(startuse, 'YYYY-MM-DD') as ""startuse"",
                        to_char(enduse, 'YYYY-MM-DD') as ""enduse""
                from public.""Cars""
                where vin = @vin
            ";

            DataTable table = new DataTable();
            string sqlDataSource = _configuration.GetConnectionString("CarsAppCon");
            NpgsqlDataReader myReader;
            using (NpgsqlConnection myCon = new NpgsqlConnection(sqlDataSource))
            {
                myCon.Open();
                using (NpgsqlCommand myCommand = new NpgsqlCommand(query, myCon))
                {
                    myCommand.Parameters.AddWithValue("@vin", VIN);
                    myReader = myCommand.ExecuteReader();
                    table.Load(myReader);

                    myReader.Close();
                    myCon.Close();
                }
            }
            return new JsonResult(table);
        }
        #endregion

        [HttpPut]
        [Route("PutDecommissionCar")]
        #region put method
        //PUT http://localhost:62841/Cars
        //update only enduse(Дата окончания эксплуатации)
        public JsonResult PutDecommissionCar(Cars car)
        {
            string query = @"
                update public.""Cars""
                set enduse = @enduse
                where id = @id
            ";

            DataTable table = new DataTable();
            string sqlDataSource = _configuration.GetConnectionString("CarsAppCon");
            NpgsqlDataReader myReader;
            using (NpgsqlConnection myCon = new NpgsqlConnection(sqlDataSource))
            {
                myCon.Open();
                using (NpgsqlCommand myCommand = new NpgsqlCommand(query, myCon))
                {
                    myCommand.Parameters.AddWithValue("@id", car.id);
                    myCommand.Parameters.AddWithValue("@enduse", Convert.ToDateTime(car.enduse));
                    myReader = myCommand.ExecuteReader();
                    table.Load(myReader);

                    myReader.Close();
                    myCon.Close();
                }
            }
            return new JsonResult("Updated successfully!");
        }
        #endregion




    }
}
